***Settings***
Documentation       Representacao da pagina login com suas açoes e elementos

***Keywords***
Login With
    [Arguments]     ${email}    ${password}
    input text      id:txtEmail                         ${email}
    input text      css:input[placeholder=Senha]        ${password}
    click Element   xpath://button[text()="Entrar"]
