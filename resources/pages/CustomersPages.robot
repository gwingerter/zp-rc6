***Settings***

Documentation           Representação da página de clientes e seus componentes e ações


***Variables***

${CUSTOMERS_FORMS}             css:a[href$=register]   

${LABEL_NOME}                 css: label[for=name]         
${LABEL_CPF}                  css: label[for=cpf]          
${LABEL_ENDEREÇO}             css: label[for=address]      
${LABEL_PHONE}                css: label[for=phone_number] 
${CSS_TABLE}                  css:table 

***Keywords***
Register New Customers 
    [Arguments]     ${name}     ${cpf}  ${address}  ${phone_number}

    Input Text      id:name          ${name}
    Input Text      id:cpf           ${cpf}
    Input Text      id:address       ${address}
    Input Text      id:phone_number  ${phone_number}

    Click Element   xpath://button[text()='CADASTRAR']

Go to Customers details
    [Arguments]       ${cpf_formatado}  
    ${element}       Set Variable         xpath://td[text()="${cpf_formatado}"]             
    
    Wait Until Element Is Visible           ${element}     5
    Click Element                           ${element}

Click Remove Customers
    ${element}         Set Variable         xpath://button[text()="APAGAR"]

    Wait Until Element Is Visible           ${element}          5
    Click Element                           ${element} 