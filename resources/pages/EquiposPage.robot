***Settings***

Documentation           Representação da página de equipamentos e seus componentes e ações


***Variables***

${EQUIPOS_FORMS}             css:a[href$=register]  

${LABEL_NOME_EQUIPO}         css: label[for=equipo-name]         
${LABEL_PRECO}               css: label[for=daily_price]          



***Keywords***
Register New Equipos
    [Arguments]                ${name}     ${preco}

    Input Text     id:equipo-name    ${name}
    Input Text     id:daily_price    ${preco}

    Click Element   xpath://button[text()='CADASTRAR']

    




