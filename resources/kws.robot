***Keywords***
Acesso a página de Login
    Go To           ${base_url}        

Submeto minhas credenciais
    [Arguments]     ${email}    ${password}
    Login With      ${email}    ${password}

Deve ver a area logada
    Wait Until Page Contains         Aluguéis      5     

Entao devo ver a mensagem de erro de cadastro:     
    [Arguments]     ${expect_notice}  

    Wait Until Element Contains  ${ERROR_TOASTER}           ${expect_notice}  

Então devo ver mensagens informando que os campos do cadastro de equipamentos são obrigatório
   Wait Until Element Contains       ${LABEL_NOME_EQUIPO}        Nome do equipo é obrigatório         5
   Wait Until Element Contains       ${LABEL_PRECO}              Diária do equipo é obrigatória       5

Entao devo ver a mensagem do erro 
   [Arguments]                   ${expect_text}  
    Wait Until Page Contains     ${expect_text}          5 

Dado que acesso o formulario de cadastro de clientes
    Go To Customers  
    Wait Until Element Is Visible             ${CUSTOMERS_FORMS}          5 
    Click Element                             ${CUSTOMERS_FORMS}  

E que eu tenho o seguinte cliente:
    [Arguments]     ${name}     ${cpf}       ${address}     ${phone_number}
    Remove Customers By Cpf     ${cpf}       
    Set Test Variable           ${name}   
    Set Test Variable           ${cpf}  
    Set Test Variable           ${address}   
    Set Test Variable           ${phone_number}  

Mas esse cpf já existe no sistema
    Insert Customers     ${name}     ${cpf}    ${phone_number}  ${address}   
    
Quando faço a inclusão de cliente
    Register New Customers  ${name}     ${cpf}  ${address}      ${phone_number}    

Entao devo ver a notificação:   
    [Arguments]     ${expect_notice}

    Wait Until Element Contains     ${SUCESS_TOASTER}       ${expect_notice}    

Entao devo ver a mensagem de erro 

    [Arguments]     ${expect_notice}

    Wait Until Element Contains     ${ERROR_TOASTER}         ${expect_notice}        

Então devo ver mensagens informando que os campos do cadastro de clientes são obrigatório
   
   Wait Until Element Contains   ${LABEL_NOME}       Nome é obrigatório      5
   Wait Until Element Contains   ${LABEL_CPF}        CPF é obrigatório       5
   Wait Until Element Contains   ${LABEL_ENDEREÇO}   Endereço é obrigatório  5
   Wait Until Element Contains   ${LABEL_PHONE}      Telefone é obrigatório  5

Então devo ver o texto:
    [Arguments]                  ${expect_text}  
    Wait Until Page Contains     ${expect_text}          5 


E esse cliente deve ser exibido na lista 
    ${cpf_formatado}           format cpf     ${cpf}  
    Go back
    Wait Until Element Is Visible        ${CSS_TABLE}     5  
    Table should Contain                 ${CSS_TABLE}     ${cpf_formatado}

Dado que eu tenho um cliente indesejado:
    [Arguments]                 ${name}     ${cpf}    ${phone_number}        ${address}
    Remove Customers By Cpf                 ${cpf}  
    Insert Customers            ${name}     ${cpf}    ${address}             ${phone_number}
    Set Test Variable                       ${cpf}             

E acesso a lista de clientes

    Go To Customers

Quando eu removo esse cliente 

    ${cpf_formatado}=          format cpf     ${cpf}  
    Set Test Variable          ${cpf_formatado}
    
    
    Go to Customers details                   ${cpf_formatado}
    Click Remove Customers

E esse cliente não deve aparecer na lista 

    Wait Until Page Does Not Contain Element         ${cpf_formatado}

Devo ver um toaster com a mensagem
    [Arguments]    ${expect_message}   
    Wait Until Element Contains         ${FAIL_TOASTER}      ${expect_message}   

Dado que acesso o formulario de cadastro de equipamento
    Go To Equipos  
    Wait Until Page Contains Element                  ${EQUIPOS_FORMS}            5  
    Click Element                                     ${EQUIPOS_FORMS}  
    
E que eu tenho o seguinte equipamento:
    [Arguments]             ${name}     ${preco}    
    Remove equipo By Nome   ${name}   
    Set Test Variable       ${name}    
    Set Test Variable       ${preco}  

Mas esse equipamento já existe no sistema

    Insert Equipos         ${name}     ${preco} 
    
Quando faço a inclusão do equipamento

    Register New Equipos    ${name}     ${preco}





