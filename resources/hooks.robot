***Keywords***

Start Session 
#about:Blank Abrir uma página em branco
    Open Browser    about:Blank     Chrome
    Maximize Browser Window

Finish Session
    Close Browser

Login Session
    Start Session 
    Go To        ${base_url}  
    Login With   ${admin_user}     ${admin_pass} 
