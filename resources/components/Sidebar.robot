***Settings***
Documentation       Representação do menu lateral de navegação na area logada

***Variables***

${NAV_CUSTOMERS}         css:a[href$=customers]   
${NAV_EQUIPOS}           css:a[href$=equipos]    

***Keywords***
Go To Customers
    Wait Until Page Contains Element                  ${NAV_CUSTOMERS}              5      
    Click Element                                     ${NAV_CUSTOMERS }  

Go To Equipos
    Wait Until Page Contains Element                  ${NAV_EQUIPOS}              5      
    Click Element                                     ${NAV_EQUIPOS}