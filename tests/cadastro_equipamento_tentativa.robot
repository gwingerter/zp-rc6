***Settings***         
Documentation       Cadastro clientes
Resource            ../resources/base.robot
Suite Setup         Login Session
Suite Teardown      Finish Session

***Test Cases***
Campos Obrigatório 
    [Tags]      temp    
    Dado que acesso o formulario de cadastro de equipamento
     E que eu tenho o seguinte equipamento:
    ...                  ${EMPTY}        ${EMPTY} 
    Quando faço a inclusão do equipamento    
    Então devo ver mensagens informando que os campos do cadastro de equipamentos são obrigatório
#As templates abaixos são referentes a keyword abaixo 
Campo nome do equipamento é obrigatório
    [Tags]                  require
    [Template]              Validação de campos
    ${EMPTY}                150            Nome do equipo é obrigatório
    
Campo preço é obrigatorio 
     [Tags]                  require 
     [Template]            Validação de campos
     Bateria               ${EMPTY}       Diária do equipo é obrigatória

***Keywords***
Validação de campos
    [Arguments]            ${name}      ${preco}    ${saida}
    Dado que acesso o formulario de cadastro de equipamento
    E que eu tenho o seguinte equipamento:
    ...                 ${name}      ${preco}
    Quando faço a inclusão do equipamento
    Entao devo ver a mensagem do erro      ${saida}