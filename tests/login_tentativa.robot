***Settings***         
Documentation      Login tentativa   
#Acesar o arquivo base
Resource            ../resources/base.robot
#Executa uma ou mais Keywords antes somente uma vez de todos os casos de testes
Suite Setup           Start Session 
#Executa uma ou mais Keywords depois somente uma vez de todos os casos de testes
Suite Teardown      Finish Session

Test Template       Tentativa de login    

***Keywords***
Tentativa de login

    [Arguments]     ${input_email}  ${input_senha}      ${output_mensagem}

    Acesso a página de Login
    Submeto minhas credenciais             ${input_email}      ${input_senha}
    Devo ver um toaster com a mensagem     ${output_mensagem}

***Test Cases***
Senha invalida              admin@zepalheta.com.br      abc1237     Ocorreu um erro ao fazer login, cheque as credenciais. 

Email incorreto             admin@aaaaa.com.br          abc1237     Ocorreu um erro ao fazer login, cheque as credenciais.

Senha em branco             admin@zepalheta.com.br      ${EMPTY}    O campo senha é obrigatório!

Email em branco             ${EMPTY}                    pwd123      O campo email é obrigatório! 

Email e senha em branco     ${EMPTY}                    ${EMPTY}    Os campos email e senha não foram preenchidos!


