***Settings***         
Documentation       Cadastro de Equipamento
Resource            ../resources/base.robot
Suite Setup         Login Session
Suite Teardown      Finish Session

***Test Cases***
Novo Equipamento
    Dado que acesso o formulario de cadastro de equipamento
    E que eu tenho o seguinte equipamento:
    ...                 Guitarra       150
    Quando faço a inclusão do equipamento
    Entao devo ver a notificação:       Equipo cadastrado com sucesso!

Equipamento duplicado
   Dado que acesso o formulario de cadastro de equipamento 
   E que eu tenho o seguinte equipamento:
   ...          Guitarra       150
   Mas esse equipamento já existe no sistema
   Quando faço a inclusão do equipamento
   Entao devo ver a mensagem de erro de cadastro:   Erro na criação de um equipo




        







