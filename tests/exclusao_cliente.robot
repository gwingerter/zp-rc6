***Settings***         
Documentation       Exclusao de clientes
Resource            ../resources/base.robot
Suite Setup         Login Session
Suite Teardown      Finish Session

***Test Cases***
Remover cliente
    Dado que eu tenho um cliente indesejado:
    ...             Bon Dylan           33333333344          Rua dos bugs, 200       11999999999
    E acesso a lista de clientes
    Quando eu removo esse cliente 
    Entao devo ver a notificação:       Cliente removido com sucesso! 
    E esse cliente não deve aparecer na lista 


