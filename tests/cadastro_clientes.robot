***Settings***         
Documentation       Cadastro clientes
Resource            ../resources/base.robot
Suite Setup         Login Session
Suite Teardown      Finish Session

***Test Cases***
Novo Cliente  
    Dado que acesso o formulario de cadastro de clientes
    E que eu tenho o seguinte cliente:
    ...         Bom jovi        00000001406         Rua dos bugs,1000       11999999999
    Quando faço a inclusão de cliente
    Entao devo ver a notificação:    Cliente cadastrado com sucesso!
    E esse cliente deve ser exibido na lista 

Cliente Duplicado

    Dado que acesso o formulario de cadastro de clientes
    E que eu tenho o seguinte cliente:
    ...          Adrian Smith     00000001408       Rua dos bugs,2000       11999999999
    Mas esse cpf já existe no sistema
    Quando faço a inclusão de cliente
    Entao devo ver a mensagem de erro   Este CPF já existe no sistema!



