***Settings***         
Documentation       Cadastro clientes
Resource            ../resources/base.robot
Suite Setup         Login Session
Suite Teardown      Finish Session

***Test Cases***
Campos Obrigatório 
    [Tags]      temp    
    Dado que acesso o formulario de cadastro de clientes
    E que eu tenho o seguinte cliente:
    ...         ${EMPTY}        ${EMPTY}           ${EMPTY}         ${EMPTY}  
    Quando faço a inclusão de cliente    
    Então devo ver mensagens informando que os campos do cadastro de clientes são obrigatório
    
#As templates abaixos são referentes a keyword abaixo 
Campo nome é obrigatório
    [Tags]                  require
    [Template]              Validação de campos
    ${EMPTY}                73037319038          Rua dos bugs,1000       11999999999   Nome é obrigatório

Campo CPF é obrigatorio 
     [Tags]                  require 
     [Template]            Validação de campos
     Fernando Papito       ${EMPTY}       Rua dos bugs,1000       11999999999   CPF é obrigatório
Campo endereço é obrigatorio 
     [Tags]                  require
     [Template]      Validação de campos
     Fernando Papito       73037319038    ${EMPTY}                11999999999   Endereço é obrigatório
Campo numero é obrigatorio 
     [Tags]                  require
     [Template]      Validação de campos
     Fernando Papito       73037319038    Rua dos bugs,1000       ${EMPTY}      Telefone é obrigatório
     
Campo de telefone invalido 
     [Tags]                  require
     [Template]      Validação de campos
     Fernando Papito       73037319038    Rua dos bugs,1000       1195930785      Telefone inválido

***Keywords***
Validação de campos
    [Arguments]     ${Nome}     ${cpf}   ${endereço}    ${telefone}    ${saida} 
     
    Dado que acesso o formulario de cadastro de clientes
    E que eu tenho o seguinte cliente:
    ...              ${Nome}    ${cpf}   ${endereço}     ${telefone}
    Quando faço a inclusão de cliente    
    Então devo ver o texto:    ${saida}  
        


        







